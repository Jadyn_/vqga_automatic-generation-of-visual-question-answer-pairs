import sys,os
ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir)
sys.path.append(ROOT)
from utils import *
from configuration import Constant
import numpy as np
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from Attention import Seq2seqAttention

class AnswerDecoder(nn.Module):
    """
    Answer Decoder
    ======
    if training, takes initial hidden state (h_0) and answer ids (a_ids) as
    input, and outputs all the hidden states output h_1-h_{seq_len} as result:
    # also outputs logits, ids and loss
    result = AnswerDecoder(h_0, a_ids)
    ======
    if not training, takes initial hidden state h_0 as input and
    output all the hidden states output h_1-h_{seq_len} as result:
    result = AnswerDecoder(h_0, is_training=False)
    ======
    where
    h_0 have dimension: batch x hidden_size
    a_ids have dimension: batch x seq_len
    result have dimension: batch x seq_len x hidden_size
    """
    def __init__(self, input_size, hidden_size, seq_len, vocab_size, wv, dictionary, init_c=None):
        """
        :param input_size: size of word embedding
        :param hidden_size: size of hidden unit
        :param batch_size: size of each batch
        :param seq_len: length of sequence
        :param vocab_size: size of the vocabulary
        :param wv: the word embedding object (instance of torch.nn.Embedding), vocab_size x input_size
        :param init_c:
        :param init_c:
        """
        super(AnswerDecoder, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.seq_len = seq_len
        self.vocab_size = vocab_size
        self.lstm = nn.LSTMCell(input_size=input_size, hidden_size=hidden_size)
        self.input_fc = nn.Linear(2*self.hidden_size, self.hidden_size)
        self.output_fc = nn.Linear(2*self.hidden_size, self.vocab_size)
        # self.region_fc = nn.Linear(Constant.REGION_FEATURE_EMBED, self.hidden_size)
        self.wv = wv
        assert self.wv.weight.size() == (self.vocab_size, self.input_size)
        self.att_distribution = Seq2seqAttention()
        self.dictionary = dictionary

        ######################
        # Options for model
        ######################
        # Whether to use intention to initialise the hidden state or note
        self.use_intention = False

        super_categories = ['person', 'vehicle', 'food', 'animal', 'outdoor', 'appliance', 'indoor',
                            'accessory', 'sport', 'kitchen', 'furniture', 'electronic', 'number',
                            'color', 'material', 'weather', 'section', 'utility', 'object']
        self.supercates = [int(self.dictionary.word2idx[w]) for w in super_categories]

    def forward(self, h_0, es, es_emb, at_emb, a_temp=None, a_ids=None, is_training=True, set_c=None):
        # make a starting word (all of batch starts with SOS token)
        self.batch_size = es.shape[0]
        es = es.view(self.batch_size, -1)
        start_word = Variable(torch.LongTensor(np.ones(self.batch_size) * Constant.SOS)).to(Constant.DEVICE)

        # where the hidden states, logits and ids from each time step go
        output_h = []
        output_logits = []
        output_ids = []
        loss = 0

        # initialise hidden state and cell memory state
        h = h_0
        _, c = self.hidden_init()

        if is_training:
            start_word_emb = self.wv(start_word).unsqueeze(1)
            assert start_word_emb.size() == (self.batch_size, 1, self.input_size)

            targets = a_ids
            a_emb = self.wv(targets)
            assert a_emb.size() == (self.batch_size, self.seq_len, self.input_size)

            # first input
            a_in = torch.cat((start_word_emb, a_emb[:, :-1, :]), 1)
            assert a_in.size() == (self.batch_size, self.seq_len, self.input_size)
            context = torch.zeros((self.batch_size, self.hidden_size)).to(Constant.DEVICE)
            for i in range(self.seq_len):

                p_gen = Variable(torch.ones(self.batch_size)).to(Constant.DEVICE)
                for b in range(self.batch_size):
                    if a_temp[b, i] in self.supercates:
                        p_gen[b] = 0

                current_word_emb = nn.Dropout(p=Constant.DP_RATE)(a_in[:, i, :])
                input = torch.add(context, current_word_emb)
                assert input.size() == (self.batch_size, self.hidden_size)

                h, c = self.lstm(input, (h, c))
                h, c = nn.Dropout(p=Constant.DP_RATE)(h), nn.Dropout(p=Constant.DP_RATE)(c)
                assert h.size() == (self.batch_size, self.hidden_size)
                assert c.size() == (self.batch_size, self.hidden_size)
                output_h.append(h)

                att_dist = self.att_distribution(es_emb, h, at_emb)  # batch * 2num_es
                att_dist = self.mask_attn(es, att_dist)
                context_vector = (att_dist.view(self.batch_size, 4 * Constant.NUM_PAIRS, 1).repeat(1, 1, Constant.HIDDEN_DIM)
                                  * es_emb.view(self.batch_size, 4 * Constant.NUM_PAIRS, -1))
                context = torch.sum(context_vector, dim=1).squeeze()  # batch * hid
                joint = torch.cat((h, context), 1)
                gen_logits = nn.LogSoftmax(1)(self.output_fc(joint))
                logits = Variable(torch.zeros((self.batch_size, self.vocab_size), dtype=torch.float)).to(Constant.DEVICE)
                poi_logits = nn.LogSoftmax(1)(logits.scatter_(dim=1, index=es, src=att_dist))
                p_gen = p_gen.view(self.batch_size, 1).repeat(1, self.vocab_size)

                logits = torch.mul(p_gen, gen_logits) + torch.mul((1 - p_gen), poi_logits)
                assert logits.size() == (self.batch_size, self.vocab_size)

                output_logits.append(logits)
                _ , word_ids = torch.max(logits, 1)
                output_ids.append(word_ids)

            output_h = torch.stack(output_h, 1)
            assert output_h.size() == (self.batch_size, self.seq_len, self.hidden_size)
            output_logits = torch.stack(output_logits, 1)
            assert output_logits.size() == (self.batch_size, self.seq_len, self.vocab_size)
            output_ids = torch.stack(output_ids, 1)
            assert output_ids.size() == (self.batch_size, self.seq_len)

            # loss_mask = []
            # for i in range(self.batch_size):
            #     eos_ids = (output_ids[i] == Constant.EOS).nonzero()
            #     if len(eos_ids) == 0:
            #         loss_mask.append(torch.LongTensor(np.ones(self.seq_len)).to(Constant.DEVICE))
            #     else:
            #         arr = np.ones(self.seq_len)
            #         arr[eos_ids.cpu().data.numpy()[0, 0] + 1:] = 0
            #         loss_mask.append(torch.LongTensor(arr).to(Constant.DEVICE))
            #
            # loss_mask = Variable(torch.stack(loss_mask).type(torch.FloatTensor)).to(Constant.DEVICE)
            # assert loss_mask.size() == (self.batch_size, self.seq_len)
            # assert loss_mask.requires_grad == False

            loss = nn.NLLLoss(ignore_index=Constant.EOS, reduction='sum')(torch.transpose(output_logits, 1, 2), targets)
            # assert loss_matrix.size() == (self.batch_size, self.seq_len)
            # assert loss_matrix.requires_gra
            # loss = torch.sum(loss_matrix * loss_mask)
            assert loss.requires_grad

        else:
            current_word = start_word
            context = torch.zeros((self.batch_size, self.hidden_size)).to(Constant.DEVICE)
            for i in range(self.seq_len):

                p_gen = Variable(torch.ones(self.batch_size)).to(Constant.DEVICE)
                current_word_emb = self.wv(current_word)

                for b in range(self.batch_size):
                    if a_temp[b, i] in self.supercates:
                        p_gen[b] = 0
                    else:
                        current_word_emb[b] = self.wv(a_temp[b, i])

                input = torch.add(context, current_word_emb)
                h, c = self.lstm(input, (h, c))
                assert h.size() == (self.batch_size, self.hidden_size)
                assert c.size() == (self.batch_size, self.hidden_size)
                output_h.append(h)

                att_dist = self.att_distribution(es_emb, h, at_emb)  # batch * 2num_es
                att_dist = self.mask_attn(es, att_dist)
                context_vector = (att_dist.view(self.batch_size, 4 * Constant.NUM_PAIRS, 1).repeat(1, 1, Constant.HIDDEN_DIM)
                                  * es_emb.view(self.batch_size, 4 * Constant.NUM_PAIRS, -1))
                context = torch.sum(context_vector, dim=1).squeeze()  # batch * hid
                # joint = torch.cat((h, context), 1)
                # gen_logits = nn.LogSoftmax(1)(self.output_fc(joint))
                logits = Variable(torch.zeros((self.batch_size, self.vocab_size), dtype=torch.float)).to(Constant.DEVICE)
                poi_logits = nn.LogSoftmax(1)(logits.scatter_(dim=1, index=es, src=att_dist))
                p_gen = p_gen.view(self.batch_size, 1).repeat(1, self.vocab_size)

                # logits = torch.mul(p_gen, gen_logits) + torch.mul((1 - p_gen), poi_logits)
                logits = poi_logits
                for j, k in enumerate(a_temp[:, i]):
                    if p_gen[j, k]:
                        logits[j, :] = -1
                        logits[j, k] = 0


                assert logits.size() == (self.batch_size, self.vocab_size)
                output_logits.append(logits)
                _, current_word = torch.max(logits, 1)
                assert current_word.size() == (self.batch_size,)
                output_ids.append(current_word)

            output_h = torch.stack(output_h, 1)
            assert output_h.size() == (self.batch_size, self.seq_len, self.hidden_size)
            output_logits = torch.stack(output_logits, 1)
            assert output_logits.size() == (self.batch_size, self.seq_len, self.vocab_size)
            output_ids = torch.stack(output_ids, 1)
            assert output_ids.size() == (self.batch_size, self.seq_len)
            pass

        return output_h, output_logits, output_ids, loss

    def hidden_init(self):
        self.init_c = torch.zeros(self.batch_size, self.hidden_size).to(Constant.DEVICE)
        self.init_h = torch.zeros(self.batch_size, self.hidden_size).to(Constant.DEVICE)
        return self.init_h, self.init_c

    def mask_attn(self, es, att):
        mask = np.ones((self.batch_size, 4 * Constant.NUM_PAIRS))
        for i in range(self.batch_size):
            idx = (es[i] == Constant.UNK).nonzero().cpu()
            mask[i, idx] = 0
        masked_attn = att * torch.from_numpy(mask).float().to(Constant.DEVICE)
        att_dist = torch.div(masked_attn, torch.sum(masked_attn, dim=1).unsqueeze(1).repeat(1, 4 * Constant.NUM_PAIRS))

        return att_dist

