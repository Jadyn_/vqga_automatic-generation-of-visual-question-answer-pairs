import os
import json
import pickle
import numpy as np
import torch
from torch.utils.data import Dataset
from configuration import Constant


class Dictionary(object):
    def __init__(self, word2idx=None, idx2word=None):
        if word2idx is None:
            word2idx = {}
        if idx2word is None:
            idx2word = []
        self.word2idx = word2idx
        self.idx2word = idx2word

    @property
    def ntoken(self):
        return len(self.word2idx)

    @property
    def padding_idx(self):
        return self.word2idx['EOS']

    def tokenize(self, sentence):
        sentence = sentence.lower()
        sentence = sentence.replace(',', '').replace('?', '').replace('\'s', ' \'s')
        words = sentence.split()
        tokens = []
        for w in words:
            if w in self.word2idx:
                tokens.append(self.word2idx[w])
            else:
                tokens.append(self.add_word(w))
        return tokens

    def dump_to_file(self, path):
        pickle.dump([self.word2idx, self.idx2word], open(path, 'wb'))
        print('dictionary dumped to %s' % path)

    @classmethod
    def load_from_file(cls, path):
        # print('loading dictionary from %s' % path)
        word2idx, idx2word = pickle.load(open(path, 'rb'))
        d = cls(word2idx, idx2word)
        return d

    def add_word(self, word):
        if word not in self.word2idx:
            self.idx2word.append(word)
            self.word2idx[word] = len(self.idx2word) - 1
        return self.word2idx[word]

    def __len__(self):
        return len(self.idx2word)


def _create_entry(qa, es_pairs):

    lang_entry = {
        'image_id': qa['image_id'],
        'qa_id': qa['qa_id'],
        'question': qa['question'],
        'answer': qa['answer']}

    visual_entry = {
        'image_id': qa['image_id'],
        'qa_id': qa['qa_id'],
        'es': []}

    for es in es_pairs:
        if 'attributes' not in es:
            es['attributes'] = ['unknown']
        visual_entry['es'].append({'entity': ' '.join(str(word) for word in es['names']),
                                   'attribute': ' '.join(str(word) for word in es['attributes'])})

    return {'lang': lang_entry, 'visual': visual_entry}


def _load_dataset(dataroot, name):

    assert name in ['train', 'test']

    dataset_path = os.path.join(dataroot, 'question_answers.json')
    language_data = json.load(open(dataset_path))
    dataset_path = os.path.join(dataroot, 'attributes.json')
    visual_data = json.load(open(dataset_path))
    assert len(language_data) == len(visual_data)
    split = int(0.8 * len(language_data))

    if name == 'train':
        language_data = language_data[:split]
        visual_data = visual_data[:split]
    else:
        language_data = language_data[split:]
        visual_data = visual_data[split:]

    entries = []
    for lang, img in zip(language_data, visual_data):
        assert lang['id'] == img['image_id']
        es_pairs = img['attributes']
        for qa in lang['qas']:
            entries.append(_create_entry(qa, es_pairs))

    return entries


class VGDataset(Dataset):
    def __init__(self, name, dictionary, use_dev=False, dataroot='data/VG/'):
        super(VGDataset, self).__init__()
        assert name in ['train', 'test']

        dataset_path = os.path.join(dataroot, 'question_answers.json')
        self.dictionary = dictionary
        if use_dev:
            self.all_data = json.load(open(dataset_path))[:250]
            self.entries = pickle.load(open(dataroot + 'train_entries_dev.pkl', 'rb'))
        else:
            self.all_data = json.load(open(dataset_path))
            self.entries = pickle.load(open(dataroot + name + '_entries_VG.pkl', 'rb'))
        # for tok in ['SOS', 'EOS','unknown']:
        #     print(tok, self.dictionary.add_word(tok))

        # self.entries = _load_dataset(dataroot, name)
        # print('finish loading')
        # self.tokenize()
        # print('tokenized')
        # self.dictionary.dump_to_file(dataroot+'VG_dictionary.pkl')
        # self.save_entries(dataroot+name+'_entries_VG.pkl')

        # self.tensorize()

    def tokenize(self, max_length=16):
        """Tokenizes the questions.

        This will add q_token in each entry of the dataset.
        EOS should be treated as padding_idx in embedding

        len of qa: 16
        len of one es: 4
        len of es pairs per img: 50

        """


        def padding(tokens, type, max_length):
            assert type in ['qa', 'es', 'pairs']
            if type == 'qa':
                padding_idx = self.dictionary.padding_idx
            elif type == 'es':
                padding_idx = self.dictionary.word2idx['unknown']
            else:
                padding_idx = [self.dictionary.word2idx['unknown']] * 4

            if len(tokens) < max_length:
                padding = [padding_idx] * (max_length - len(tokens))
                tokens = tokens + padding
            else:
                tokens = tokens[:max_length]
            assert len(tokens) == max_length
            return tokens

        for entry in self.entries:

            # tokenize qa pairs
            qa = entry['lang']
            tokens = self.dictionary.tokenize(qa['question'])
            qa['q_token'] = padding(tokens, 'qa', max_length)
            tokens = self.dictionary.tokenize(qa['answer'])
            qa['a_token'] = padding(tokens, 'qa', max_length)

            # tokenize es pairs
            es_pairs = []
            ess = entry['visual']['es']
            for es in ess:
                es['entity_token'] = self.dictionary.tokenize(es['entity'])
                es['attribute_token'] = self.dictionary.tokenize(es['attribute'])
                es_pair = padding(es['entity_token'] + es['attribute_token'], 'es', 4)  # in dataset most is 5
                es_pairs.append(es_pair)

            qa['es_token'] = padding(es_pairs, 'pairs', 50)

    def tensorize(self, question, answer, es, img_id):

        question = torch.LongTensor(question).to(Constant.DEVICE)
        answer = torch.LongTensor(answer).to(Constant.DEVICE)
        es = torch.LongTensor(es).to(Constant.DEVICE)

        return question, answer, es, img_id

    def save_entries(self, path):
        pickle.dump(self.entries, open(path, 'wb'))
        print('entries dumped to %s' % path)

    def __getitem__(self, index):
        entry = self.entries[index]['lang']
        question = entry['q_token']
        answer = entry['a_token']
        es = entry['es_token']
        img_id = entry['image_id']

        return self.tensorize(question, answer, es, img_id)
        # return question, answer, es, img_id

    def __len__(self):
        return len(self.entries)


def create_glove_embedding_init(d, glove_file):
    word2emb = {}
    with open(glove_file, 'r') as f:
        entries = f.readlines()
    emb_dim = len(entries[0].split(' ')) - 1
    print('embedding dim is %d' % emb_dim)
    weights = np.zeros((len(d.idx2word), emb_dim), dtype=np.float32)

    for entry in entries:
        vals = entry.split(' ')
        word = vals[0]
        vals = list(map(float, vals[1:]))
        word2emb[word] = np.array(vals)
    for idx, word in enumerate(d.word2idx):
        if word not in word2emb:
            continue
        weights[idx] = word2emb[word]
    return weights, word2emb


# if __name__ == "__main__":
#     dictionary = Dictionary.load_from_file('data/VG/VG_dictionary.pkl')
#     train_dset = VGDataset('train', dictionary)
#     test_dset = VGDataset('test', dictionary)
#     print(len(dictionary))

    # d = Dictionary.load_from_file('data/VG/VG_dictionary.pkl')
    # glove_file = 'data/glove.42B.300d.txt'
    # weights, word2emb = create_glove_embedding_init(d, glove_file)
    # np.save('data/glove42b_weights_300d.npy', weights)
    # np.save('data/glove42b_embed_300d.npy', word2emb)
    # dataroot = 'data/VG/'
    # dataset_path = os.path.join(dataroot, 'question_answers.json')
    # language_data = json.load(open(dataset_path))
    # split = int(0.8 * len(language_data))
    #
    # language_data = language_data[split:]
    #
    # entries = []
    # for lang in language_data:
    #     for qa in lang['qas']:
    #         if qa['qa_id'] == 529990:
    #             print(qa['question'])

