from configuration import Constant
import torch
import torch.nn.functional as F
import numpy as np


def g(es, q_emb):
    """
    :param es: entity-attribute pairs embedding
    :param q_emb: question embedding
    :return: joint representation: g(e,s,Q)
    """

    # phase 1

    # es_test = torch.zeros(Constant.NUM_PAIRS, Constant.BATCH_SIZE, 2, Constant.HIDDEN_DIM)
    # q_ = torch.zeros(Constant.BATCH_SIZE, Constant.MAX_SEQ_LEN, Constant.HIDDEN_DIM)
    # q_test = torch.unsqueeze(q_, 0).repeat(Constant.NUM_PAIRS, 1, 1, 1)
    # U = torch.matmul(es_test, torch.transpose(q_test, 2, 3))
    # A_q = F.softmax(U, dim=2)
    # A_es = F.softmax(U, dim=3)
    # Q_norm = torch.matmul(A_q, q_test)
    # E_norm = torch.matmul(torch.transpose(U, 2, 3), es_test)
    # U_sum = torch.sum(U, 3)
    # hQ = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 2), Q_norm))
    # U_sum = torch.sum(U, 2)
    # hE = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 2), E_norm))

    # ==================

    pair_embedding = es.view(Constant.BATCH_SIZE, 2, -1)

    U = torch.matmul(pair_embedding, torch.transpose(q_emb, 1, 2))
    A_q = F.softmax(U, dim=1)
    A_es = F.softmax(U, dim=0)
    assert U.size() == (Constant.BATCH_SIZE, 2, Constant.MAX_SEQ_LEN)  # 2 for one ES pair

    Q_norm = torch.matmul(A_q, q_emb)
    E_norm = torch.matmul(torch.transpose(A_es, 1, 2), pair_embedding)
    assert Q_norm.size() == (Constant.BATCH_SIZE, 2, Constant.HIDDEN_DIM)
    assert E_norm.size() == (Constant.BATCH_SIZE, Constant.MAX_SEQ_LEN, Constant.HIDDEN_DIM)

    U_sum = torch.sum(U, 2)
    hQ = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 1), Q_norm))
    U_sum = torch.sum(U, 1)
    hE = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 1), E_norm))
    assert hQ.size() == (Constant.BATCH_SIZE, Constant.HIDDEN_DIM)
    assert hQ.size() == hE.size()

    return hQ, hE


def h_att(E, q_emb):
    """

    :param E: entity-attribute pairs embedding
    :param q_emb: question embedding
    :return: h_{e,s,Q}
    """
    # phase 2

    E = torch.transpose(E.view(-1, Constant.NUM_PAIRS, 4, Constant.HIDDEN_DIM), 0, 1)
    q_emb = torch.unsqueeze(q_emb, 0).repeat(Constant.NUM_PAIRS, 1, 1, 1)
    U = torch.matmul(E, torch.transpose(q_emb, 2, 3))
    A_q = F.softmax(U, dim=2)
    A_es = F.softmax(U, dim=3)
    Q_norm = torch.matmul(A_q, q_emb)
    E_norm = torch.matmul(torch.transpose(A_es, 2, 3), E)
    U_sum = torch.sum(U, 3)
    hQ = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 2), Q_norm))
    U_sum = torch.sum(U, 2)
    hE = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 2), E_norm))

    ###############

    G = torch.cat((hQ, hE), dim=2)
    score = torch.matmul(torch.unsqueeze(hE, 2), torch.unsqueeze(hQ, 3))
    score = F.softmax(torch.squeeze(score, dim=-1), dim=0).repeat(1, 1, G.size()[-1])
    # h_att = torch.squeeze(torch.sum(G * score.expand_as(G), 0))
    h_esq = torch.sum(G*score, 0)

    ##############

    #test

    # E_np = np.ones((Constant.NUM_PAIRS, Constant.BATCH_SIZE, 2, Constant.HIDDEN_DIM))
    # q_np = 2*np.ones((Constant.NUM_PAIRS, Constant.BATCH_SIZE, Constant.MAX_SEQ_LEN, Constant.HIDDEN_DIM))
    # E = torch.from_numpy(E_np)
    # q_emb = torch.from_numpy(q_np)
    # U_np = np.matmul(E_np, q_np.transpose((0, 1, 3, 2)))
    # U = torch.matmul(E, torch.transpose(q_emb, 2, 3))
    # np.alltrue(U_np == U.numpy())
    #
    # A_q = F.softmax(U, dim=2)
    # A_es = F.softmax(U, dim=3)
    # Q_norm = torch.matmul(A_q, q_emb)
    # E_norm = torch.matmul(torch.transpose(A_es, 2, 3), E)
    # Q_np = np.matmul(A_q.numpy(), q_np)
    # E_np = np.matmul(A_es.numpy().transpose((0, 1, 3, 2)), E_np)
    # np.alltrue(Q_np == Q_norm.numpy())
    # np.alltrue(E_np == E_norm.numpy())
    #
    # U_sum = torch.sum(U, 3)
    # hQ = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 2), Q_norm))
    # U_sum = torch.sum(U, 2)
    # hE = torch.squeeze(torch.matmul(torch.unsqueeze(U_sum, 2), E_norm))
    # G = torch.cat((hQ, hE), dim=2)
    # score = torch.matmul(torch.unsqueeze(hE, 2), torch.unsqueeze(hQ, 3))
    # score = F.softmax(torch.squeeze(score, dim=-1), dim=1)
    # h_att = torch.squeeze(torch.sum(G * score.expand_as(G), 0))
    # h_att_single = G[0, 0, :] * score[0, 0, 0]
    # h_single_np = G.numpy()[0, 0, :] * score.numpy()[0, 0, 0]
    # np.alltrue(h_single_np == h_att_single.numpy())
    # assert 84*h_single_np[0] == h_att[0, 0]

    # end of test

    # if Constant.USE_CUDA:
    #     h_att = torch.zeros(Constant.BATCH_SIZE, 2*Constant.HIDDEN_DIM).cuda()
    # else:
    #     h_att = torch.zeros(Constant.BATCH_SIZE, 2 * Constant.HIDDEN_DIM)

    # for id in range(Constant.NUM_PAIRS):  # reshape to batch version & .view back
    #     es = torch.squeeze(E[:, id, :])
    #     hQ, hE = g(es, q_emb)
    #     G = torch.cat((hQ, hE), dim=1)
    #     assert G.size() == (Constant.BATCH_SIZE, 2*Constant.HIDDEN_DIM)
    #     score = []
    #     for i in range(Constant.BATCH_SIZE):
    #         score.append(torch.matmul(hQ[i], hE[i]))
    #     if Constant.USE_CUDA:
    #         score = F.softmax(torch.FloatTensor(score).cuda(), dim=0)  # size: Constant.BATCH_SIZE
    #     else:
    #         score = F.softmax(torch.FloatTensor(score), dim=0)
    #     single_att = []
    #     for i in range(Constant.BATCH_SIZE):
    #         single_att.append(G[i]*score[i])
    #     if Constant.USE_CUDA:
    #         single_att = torch.stack(single_att).cuda()
    #     else:
    #         single_att = torch.stack(single_att)
    #     h_att = torch.add(h_att, 1, single_att)

    return h_esq

