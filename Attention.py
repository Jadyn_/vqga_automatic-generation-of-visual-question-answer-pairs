import sys, os
ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir)
sys.path.append(ROOT)
from utils import *
from configuration import Constant
import torch
import torch.nn as nn
from torch.autograd import Variable


class TopDownAttention(nn.Module):
    # (https://arxiv.org/pdf/1707.07998.pdf)

    def __init__(self, m, n):
        super(TopDownAttention, self).__init__()
        self.attn_tanh = nn.Linear(m, n)
        self.attn_sigm = nn.Linear(m, n)
        self.w_a = torch.FloatTensor(Constant.BATCH_SIZE, 1, Constant.NUM_PAIRS).to(Constant.DEVICE)
        self.norm = nn.Sigmoid()
        torch.nn.init.xavier_normal_(self.w_a)

    def forward(self, q, v):
        self.batch_size = q.shape[0]
        self.w_a = self.w_a[:self.batch_size]
        qv = torch.cat((v, q), dim=2)
        y_ = nn.Tanh()(self.attn_tanh(qv))
        g = nn.Sigmoid()(self.attn_sigm(qv))
        y = nn.Dropout(p=Constant.DP_RATE)(y_ * g)
        logits = torch.matmul(self.w_a, y).squeeze()
        return self.norm(logits)


class QuestionAttention(nn.Module):

    def __init__(self, input_size, hidden_size, num_layers, kernel_size, map_size, num_temps, vocab_size, dropout=0, bidirectional=True):
        super(QuestionAttention, self).__init__()
        self.hidden_size = hidden_size
        self.map_size = map_size
        self.kernel_size = kernel_size  # kernel_size = num_es
        self.num_temps = num_temps
        self.lstm = nn.LSTM(input_size, self.hidden_size, num_layers=num_layers, dropout=dropout, bidirectional=bidirectional, batch_first=True)
        self.reduce_hidden = nn.Sequential(nn.Linear(2*hidden_size, hidden_size), nn.ReLU())
        self.linear = nn.Linear(self.map_size, self.hidden_size)
        self.decoder = nn.Linear(self.hidden_size, vocab_size)

        self.temp_batch = 512
        self.context = Variable(torch.FloatTensor(Constant.BATCH_SIZE, num_temps, hidden_size)).to(Constant.DEVICE)
        self.weights = Variable(torch.FloatTensor(1, hidden_size, hidden_size)).to(Constant.DEVICE)
        self.b_cont = Variable(torch.zeros(Constant.BATCH_SIZE, 1)).to(Constant.DEVICE)
        torch.nn.init.xavier_normal_(self.context)
        torch.nn.init.xavier_normal_(self.weights)

    def forward(self, seq_in, answer, feature_map):

        self.batch_size = feature_map.shape[0]
        self.context = self.context[:self.batch_size]
        self.b_cont = self.b_cont[:self.batch_size]

        question_hidden_all, answer_hidden_all = self.template_encoder(seq_in, answer)
        w_qt = question_hidden_all.repeat(self.batch_size, 1, 1)  # (batch, num_temps, hidden_size)
        v = self.linear(feature_map)  # batch * pairs * hidden
        dot_prod_att = torch.bmm(self.context.view(-1, self.num_temps, Constant.HIDDEN_DIM), v.view(-1, Constant.HIDDEN_DIM, self.kernel_size))
        # a_q = nn.Tanh()(dot_prod_att)
        a_q = (dot_prod_att)
        a_q_norm = nn.Softmax(dim=2)(a_q)  # batch * num_temps * pairs
        h_q = torch.matmul(a_q_norm, v)  # batch * num_temps * hidden
        dot_prod_sig = torch.bmm(w_qt.view(-1, 1, Constant.HIDDEN_DIM), self.weights.repeat(self.batch_size*Constant.NUM_TEMPS, 1, 1))
        dot_prod_sig = torch.bmm(dot_prod_sig, h_q.view(-1, Constant.HIDDEN_DIM, 1))
        # temps_score = nn.Sigmoid()(dot_prod_sig.view(Constant.BATCH_SIZE, self.num_temps) + self.b_cont.repeat(1, self.num_temps))
        temps_score = (dot_prod_sig.view(self.batch_size, self.num_temps) + self.b_cont.repeat(1, self.num_temps))
        return temps_score, question_hidden_all, answer_hidden_all

    def template_encoder(self, question, answer):

        question_hidden_all = []
        answer_hidden_all = []
        for i in range(0, Constant.NUM_TEMPS, self.temp_batch):
            length = min(i+self.temp_batch, Constant.NUM_TEMPS)
            q_input = question[:, i:length, :]
            a_input = answer[:, i:length, :]
            h0, c0 = self.init_hidden(length - i)
            self.lstm.flatten_parameters()
            output, (question_hidden, c) = self.lstm(q_input, (h0, c0))
            self.lstm.flatten_parameters()
            output, (answer_hidden, c) = self.lstm(a_input, (h0, c0))
            reduced_question_hidden = self.reduce_hidden(question_hidden.view(-1, 2*self.hidden_size))
            reduced_answer_hidden = self.reduce_hidden(answer_hidden.view(-1, 2*self.hidden_size))
            question_hidden_all.append(reduced_question_hidden)
            answer_hidden_all.append(reduced_answer_hidden)

        question_hidden_all = torch.cat(question_hidden_all, 0)  # h: (2, num_temps, hidden)
        answer_hidden_all = torch.cat(answer_hidden_all, 0)

        return question_hidden_all, answer_hidden_all

    def init_hidden(self, batch):

        weight = next(self.parameters()).data
        hid_shape = (2, batch, self.hidden_size)
        return Variable(weight.new(*hid_shape).zero_()), Variable(weight.new(*hid_shape).zero_())


class Seq2seqAttention(nn.Module):
    # https://arxiv.org/abs/1704.04368
    def __init__(self):
        super(Seq2seqAttention, self).__init__()
        self.output = nn.Softmax(dim=1)

        self.v = Variable(torch.FloatTensor(Constant.BATCH_SIZE, 1, 1, Constant.HIDDEN_DIM)).to(Constant.DEVICE)
        self.w_h = Variable(torch.FloatTensor(Constant.BATCH_SIZE, 1, 1)).to(Constant.DEVICE)
        self.w_s = nn.Linear(Constant.HIDDEN_DIM, Constant.HIDDEN_DIM)
        self.w_t = nn.Linear(Constant.HIDDEN_DIM, Constant.HIDDEN_DIM)

        torch.nn.init.xavier_normal_(self.v)
        torch.nn.init.xavier_normal_(self.w_h)

    def forward(self, H, s, t):
        """

        :param H: batch * 4num_es * emb
        :param s: batch * emb
        :param t: batch * emb
        :return: batch * 4num_es
        """
        # batch * 4num_es * emb
        #                   + self.b_att.repeat(1, 4*Constant.NUM_PAIRS, 1))

        self.batch_size = H.shape[0]
        self.v = self.v[:self.batch_size]
        self.w_h = self.w_h[:self.batch_size]



        joint = nn.Tanh()(torch.mul(self.w_h.repeat(1, 4 * Constant.NUM_PAIRS, Constant.HIDDEN_DIM), H).squeeze() +
                          self.w_s(s).unsqueeze(1).repeat(1, 4 * Constant.NUM_PAIRS, 1) +
                          self.w_t(t).unsqueeze(1).repeat(1, 4 * Constant.NUM_PAIRS, 1))

        e = torch.bmm(self.v.repeat(1, 4*Constant.NUM_PAIRS, 1, 1).view(4*self.batch_size*Constant.NUM_PAIRS, 1, -1),
                      joint.view(self.batch_size*4*Constant.NUM_PAIRS, Constant.HIDDEN_DIM, 1)).squeeze() # batch * 4num_es
        e = e.view(self.batch_size, 4*Constant.NUM_PAIRS)
        att = self.output(e)
        return att


class deprecated_QuestionAttention(nn.Module):

    def __init__(self, input_size, hidden_size, num_layers, kernel_size, map_size, num_temps, vocab_size, dropout=0, bidirectional=False):
        super(deprecated_QuestionAttention, self).__init__()
        self.map_size = map_size
        self.kernel_size = kernel_size  # kernel_size = num_es
        self.num_temps = num_temps
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers=num_layers, dropout=dropout, bidirectional=bidirectional)
        self.mean = nn.AvgPool1d(self.kernel_size)
        self.linear = nn.Linear(self.map_size, hidden_size)
        self.decoder = nn.Linear(hidden_size, vocab_size)
        if Constant.USE_CUDA:
            self.init_c = Variable(torch.FloatTensor(1, num_temps, hidden_size)).cuda()
            self.init_h = Variable(torch.FloatTensor(1, num_temps, hidden_size)).cuda()
        else:
            self.init_c = Variable(torch.FloatTensor(1, num_temps, hidden_size))
            self.init_h = Variable(torch.FloatTensor(1, num_temps, hidden_size))
        torch.nn.init.xavier_normal_(self.init_c)
        torch.nn.init.xavier_normal_(self.init_h)

    def forward(self, seq_in, targets, feature_map):
        """

        :param seq_in: seq_len, num_temps, emb
        :param feature_map: batch, 2*emb, num_es
        :return: batch, num_temps
        """

        h = self.init_h
        c = self.init_c
        output, (h, c) = self.lstm(seq_in, (h, c))  # h: (num_layers * num_directions, num_temps, hidden_size)
        # logits = nn.LogSoftmax(2)(self.decoder(output))
        # loss = nn.NLLLoss(reduction='none')(logits.view(self.num_temps, -1, len(output)), targets)
        # loss = torch.sum(loss)
        # assert loss.requires_grad
        w_qt = h.squeeze().repeat(Constant.BATCH_SIZE, 1, 1)  # (batch, num_temps, hidden_size)
        v = self.mean(feature_map.view(Constant.BATCH_SIZE, self.map_size, self.kernel_size))  # (batch, 2*emb, 1)
        v_mean = self.linear(v.squeeze()).unsqueeze(1).repeat(1, self.num_temps, 1)  # (batch, num_temps, hidden_size)
        inner = torch.bmm(w_qt.view(-1, 1, Constant.HIDDEN_DIM), v_mean.view(-1, Constant.HIDDEN_DIM, 1))
        inner = inner.view(Constant.BATCH_SIZE, self.num_temps) * 1e5
        temps_score = nn.Sigmoid()(inner)  # (batch, num_temps)
        return temps_score, h  # , loss / self.num_temps) * Constant.BATCH_SIZE

