import json
from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
from nltk.corpus import wordnet as wn
import nltk
from VGdata import Dictionary, VGDataset
import numpy as np
import copy
# nltk.download('averaged_perceptron_tagger')
from scipy.spatial.distance import hamming
import random

def get_data(dictionary):

    super_categories = ['person', 'vehicle', 'food', 'animal', 'outdoor', 'appliance', 'indoor',
                        'accessory', 'sport', 'kitchen', 'furniture', 'electronic', 'number',
                        'color', 'material', 'weather', 'section', 'utility']

    super_semantics = [wn.synsets('person', pos=wn.NOUN), wn.synsets('vehicle', pos=wn.NOUN), wn.synsets('food', pos=wn.NOUN),
                       wn.synsets('animal', pos=wn.NOUN), wn.synsets('outdoor', pos=wn.NOUN), wn.synsets('appliance', pos=wn.NOUN),
                       wn.synsets('indoor', pos=wn.NOUN), wn.synsets('accessory', pos=wn.NOUN), wn.synsets('sport', pos=wn.VERB),
                       wn.synsets('kitchen', pos=wn.NOUN), wn.synsets('furniture', pos=wn.NOUN),
                       wn.synsets('electronic', pos=wn.ADJ),
                       wn.synsets('number', pos=wn.NOUN), wn.synsets('color', pos=wn.NOUN),
                       wn.synsets('material', pos=wn.NOUN), wn.synsets('weather', pos=wn.NOUN),
                       wn.synsets('section', pos=wn.NOUN),wn.synsets('utility', pos=wn.NOUN)
                       ]

    # for w in super_categories:
    #     if w not in dictionary.word2idx:
    #         print(w)
    #         dictionary.add_word(w)

    return super_categories, super_semantics


class Template():

    def __init__(self, dataset, supercates, supersems):
        self.dict = dataset.dictionary
        self.num_entries = len(dataset)
        self.logits_all = np.zeros(len(self.dict))
        self.dataset = dataset
        self.supercates = supercates
        self.supersems = supersems
        self.templates = []

    def cal_distribution(self):

        for i in range(self.num_entries):
            tokens = self.dataset[i][0]
            self._word_logits(tokens)

        return self.logits_all / sum(self.logits_all)

    def _word_logits(self, toks):
        for w in toks:
            self.logits_all[w] += 1

    def generate_templates(self, distribution, thres=0.7):
        word2id = self.dict.word2idx
        id2word = self.dict.idx2word
        self.word_distribution = distribution
        start_words = ['what', 'where', 'how', 'which', 'who', 'whose', 'when', 'why']
        start_ids = [word2id[w] for w in start_words]
        prep_words = ['like', 'as', 'by', 'in', 'on', 'over', 'through', 'with', 'without', 'from', 'behind', 'above']
        prep_ids = [word2id[w] for w in prep_words]

        weights = (0.25, 0.25, 0.25, 0.25)

        for i in range(self.num_entries):
            tokens = self.dataset[i][0]
            trans_q_toks = self._word_replace(tokens, 1, start_ids, prep_ids)
            trans_q_toks = [int(trans_q_toks[i]) for i in range(len(trans_q_toks))]
            trans_q = [id2word[tok] for tok in trans_q_toks]
            tokens = self.dataset[i][1]
            trans_a_toks = self._word_replace(tokens, 1, start_ids, prep_ids)
            trans_a_toks = [int(trans_a_toks[i]) for i in range(len(trans_a_toks))]
            trans_a = [id2word[tok] for tok in trans_a_toks]
            question_id = self.dataset.entries[i]['lang']['qa_id']

            if self.templates:
                temp_score = sentence_bleu([template['question'] for template in self.templates], trans_q, weights=weights,
                                           smoothing_function=SmoothingFunction().method1)
                if temp_score < thres:
                    self.templates.append({'questiontoks': trans_q_toks,
                                           'question': trans_q,
                                           'answertoks': trans_a_toks,
                                           'answer': trans_a,
                                           'question_id': question_id})
            else:
                self.templates.append({'questiontoks': trans_q_toks,
                                       'question': trans_q,
                                       'answertoks': trans_a_toks,
                                       'answer': trans_a,
                                       'question_id': question_id})

    def _semantic_score(self, a_field, b_field):
        cur_max = -1
        for a in a_field:
            for b in b_field:
                if a._pos == b._pos:
                    local_score = wn.lch_similarity(a, b)
                else:
                    continue
                if not local_score:
                    continue
                if local_score > cur_max:
                    cur_max = local_score
        return cur_max

    def _get_pos(self, tag):
        if tag[-1][0] == 'J':
            return wn.ADJ
        elif tag[-1][0] == 'V':
            return wn.VERB
        elif tag[-1][0] == 'N':
            return wn.NOUN
        elif tag[-1][0] == 'R':
            return wn.ADV
        else:
            return wn.NOUN

    def _word_replace(self, q, n, start_ids, prep_ids, freq=1e-3):
        word2id = self.dict.word2idx
        id2word = self.dict.idx2word
        q_ngram = nltk.ngrams(q, n)
        for i, ws in enumerate(q_ngram):
            ws_semantic = []
            if any([any([self.word_distribution[w] > freq for w in ws]), any([w in start_ids for w in ws]),
                     any([w in prep_ids for w in ws]), any([id2word[w] in self.supercates for w in ws])]):
                continue
            ws = [id2word[w] for w in ws]
            for w in ws:
                tag = nltk.pos_tag([w])
                w_syn = wn.synsets(w, pos=self._get_pos(tag[0]))
                ws_semantic += [w_syn[j].hypernyms()[0] for j in range(len(w_syn)) if w_syn[j].hypernyms()]
            cates_score = [self._semantic_score(cate, ws_semantic) for cate in self.supersems]
            if np.max(cates_score) > 2.:
                od = np.argmax(cates_score)
                word = self.supercates[od]
                q[i:i+n] = [word2id[word]]
            elif w_syn:
                if w_syn[0].root_hypernyms():
                    q[i:i+n] = [word2id['object']]
        return q

    # def temp2sent(self, E, T, gt_q):
    #     words_cate = dict()
    #     super_id = [str(word2id[w]) for w in self.supercates]
    #     for c in super_id:
    #         words_cate[str(c)] = []
    #     E = set([w for w in E if w != 2])
    #     word_E = [id2word[str(e)] for e in E]
    #     print('es pairs: ', word_E)
    #
    #     for e in E:
    #         e = id2word[str(e)]
    #         tag = nltk.pos_tag([e])
    #         esem = (wn.synsets(e, pos=self._get_pos(tag[0])))
    #         score = [self._semantic_score(esem, cates) for cates in self.supersems]
    #         cates_idx = np.argmax(score)
    #         if score[cates_idx] > 2:
    #             words_cate[str(super_id[cates_idx])].append(word2id[e])
    #
    #     cur_cates = [cate for cate in super_id if words_cate[str(cate)]]
    #
    #     def format_print(sent):
    #         sentence = ''
    #         for w in sent:
    #             if isinstance(w, list):
    #                 w = '[ ' + format_print(w) + '] '
    #             else:
    #                 if w != 1:
    #                     w = id2word[str(w)]
    #                 else:
    #                     w = ''
    #             sentence += w + ' '
    #         return sentence
    #
    #     def remove_padding(string):
    #         string = [id2word[str(id)] for id in string]
    #         for i in range(len(string)):
    #             if string[i] == '_':
    #                 return string[:i + 1]
    #         return string
    #
    #     fill_back = []
    #     gt_q = remove_padding(gt_q)
    #     for template in T:
    #         temp = copy.copy(template['question'])
    #         sentence_score = sentence_bleu(gt_q, remove_padding(temp),
    #                                        weights=(0.5, 0.5, 0, 0), smoothing_function=SmoothingFunction().method4)
    #         if sentence_score < 0.1:
    #             continue
    #         for i, w in enumerate(temp):
    #             if str(w) in cur_cates:
    #                 temp[i] = words_cate[str(w)]
    #
    #         arr = [str(w) not in super_id for w in temp if not isinstance(w, list)]
    #         if all(arr) and any([isinstance(w, list) for w in temp]):
    #             temp = format_print(temp)
    #             fill_back.append(temp)
    #
    #     return fill_back
    #
    # def fill_temps(self):
    #     for i in range(1):
    #         batch_indices = list(self.indicies[i * Constant.BATCH_SIZE: (i + 1) * Constant.BATCH_SIZE])
    #         batch_inputs = get_batch_arrays_from_ids(batch_indices, self.idx2indices, self.full_data)
    #         batch_questions, batch_answers, batch_pairs, batch_image_ids, batch_ids = batch_inputs
    #
    #         for j in range(batch_size):
    #             print('True Question:')
    #             print(' '.join([id2word[str(w)] for w in batch_questions[j] if w != 1]))
    #             print('#' * 10)
    #             E = np.array(batch_pairs[j]).flatten()
    #             filled = self.temp2sent(E, self.templates, batch_questions[j])
    #             for sent in filled:
    #                 print(sent)
    #             print('#'*10)


def padding(seq):
    if len(seq) > 16:
        return seq[:16]
    else:
        return seq + [1]*(16 - len(seq))


def cal_coverage(thres=0.1):
    with open('data/VG/test_temp_VG.json') as f:
        test_questions = json.load(f)
    with open('data/VG/temp_VG.json') as f:
        train_questions = json.load(f)

    num_temps = len(test_questions)
    covered = 0
    nearest_questions = {}
    tests = random.sample(range(len(test_questions)), 20)
    for temp_id in tests:
        temp = test_questions[temp_id]
        test_temp = padding(temp['questiontoks'])
        scores = []
        for target in train_questions:
            score = hamming(test_temp, padding(target['questiontoks']))
            scores += [score]
            if score < thres:
                covered += 1
                break
        nearest_ids = np.array(scores).argsort()[:5]
        nearest_questions[temp['question_id']] = {'train': [train_questions[i]['question'] for i in nearest_ids], 'test': temp['question']}
        print(nearest_questions[temp['question_id']])
        

    return covered, num_temps, covered / num_temps


if __name__ == "__main__":

    # EOS = 1
    # word_dict = Dictionary.load_from_file('data/VG/VG_dictionary.pkl')
    # super_cate, super_sem = get_data(word_dict)
    # current_dset = VGDataset('test', word_dict)
    # current_templates = Template(current_dset, super_cate, super_sem)

    # with open('model_data/word_dist_VG.json', 'w') as f:
    #     wdist = current_templates.cal_distribution()
    #     json.dump({'distribution': wdist.tolist()}, f)

    # with open('model_data/word_dist_VG.json', 'r') as f:
    #     w_dist = json.load(f)
    #
    # print('start reformulating.')
    # current_templates.generate_templates(w_dist['distribution'], thres=0.5)
    #
    # with open('test_temp_VG.json', 'w') as f:
    #     json.dump(current_templates.templates, f)
    #
    # for i, template in enumerate(current_templates.templates):
    #     print('##### ' + str(i) + '#####')
    #     print('String: ' + ' '.join([word_dict.idx2word[w] for w in template['questiontoks'] if w != EOS]))

    # with open('model_data/templates.json', 'r') as f:
    #     temp_list = json.load(f)
    # for template in temp_list:
    #     print(' '.join([id2word[str(w)] for w in template['question'] if w != 1]))


    # with open('templates_bigram.json', 'r') as f:
    #     T = json.load(f)
    # QT.templates = T
    # QT.fill_temps()

    covered, num_temps, perc = cal_coverage()
    print('covered number: ', covered)
    print('all templates: ', num_temps)
    print('coverage(percentage): ', perc)


