import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=1)
    parser.add_argument('--num_hid', type=int, default=300)
    parser.add_argument('--dropout', type=float, default=0.1)
    parser.add_argument('--output', type=str, default='saved_models/exp0')
    parser.add_argument('--batch_size', type=int, default=16)
    parser.add_argument('--test_every', type=int, default=1)
    parser.add_argument('--lr', type=float, default=1e-3)
    parser.add_argument('--seq_len', type=int, default=16, help='max seq len')
    parser.add_argument('--num_tpl', type=int, default=64, help='number of templates')
    parser.add_argument('--num_es', type=int, default=30, help='number of es pairs')
    parser.add_argument('--dataset', type=str, default='dev', help='dev or full')
    parser.add_argument('--device', type=str, default='cpu', help='cpu or cuda')
    parser.add_argument('--pretrain', type=bool, default=False, help='load pretrained model')
    args = parser.parse_args()
    return args
