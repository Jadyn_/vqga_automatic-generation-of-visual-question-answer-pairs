import sys, time
import torch.optim as optim
import random
from configuration import Constant
from utils import *
from Model import *


def train(qa_generator, train_data, test_data, templates):

    # model
    # optimizer = optim.RMSprop(qa_generator.parameters(), lr=Constant.LR, momentum=Constant.MOMENTUM)
    optimizer = optim.Adamax(qa_generator.parameters(), lr=Constant.LR)
    best_eval = 0
    use_gt = False

    for epoch in range(Constant.NUM_EPOCH):
        t = time.time()
        # qa_generator.module.train()
        qa_generator.train()

        # if pretrain:
        #     qa_generator.load_state_dict(torch.load(model_path))

        loss_q_val = 0
        loss_a_val = 0
        loss_t_val = 0
        loss_count = 0

        for i, inputs in enumerate(train_data):

            question_templates, answer_templates = templates
            template_indices = random.sample(range(len(question_templates)), Constant.NUM_TEMPS)
            question_templates = [question_templates[idx] for idx in template_indices]
            answer_templates = [answer_templates[idx] for idx in template_indices]

            # Run the model
            results = qa_generator(question_templates,
                                   answer_templates,
                                   inputs,
                                   use_gt=use_gt)

            loss, loss_q, loss_a, loss_t, output_h, output_logits, output_ids, \
            a_output_h, a_output_logits, a_output_ids, acc = results

            loss.backward()
            nn.utils.clip_grad_norm_(qa_generator.parameters(), 0.25)  # Constant.GRAD_LIMIT
            optimizer.step()
            optimizer.zero_grad()

            if not use_gt:
                loss_q_val += (loss_q / (Constant.BATCH_SIZE * Constant.MAX_SEQ_LEN)).data
            loss_a_val += (loss_a / (Constant.BATCH_SIZE * Constant.MAX_SEQ_LEN)).data
            loss_t_val += (loss_t / Constant.NUM_TEMPS).data
            loss_count += 1

        # print results
        questions, answers, es_pairs, image_ids = inputs
        if use_gt:
            q_output_id_vals = torch.LongTensor(questions)
        else:
            q_output_id_vals = output_ids[0].data.cpu()
        a_output_id_vals = a_output_ids[0].data.cpu()

        # print('temp accuracy: ', acc.item(), '%')
        print("#" * 10 + " Train Epoch {} ".format(epoch) + "#" * 10)

        c_loss = (loss_q_val + loss_a_val + loss_t_val ) / loss_count
        c_loss_q = loss_q_val / loss_count
        c_loss_a = loss_a_val / loss_count
        c_loss_t = loss_t_val / loss_count
        print(">>> training epoch: ", epoch,
              ", loss = loss_q + loss_a + loss_t : {:f} = {:f} + {:f} + {:f}".format(
                  c_loss, c_loss_q, c_loss_a, c_loss_t))
        print('true:')
        q_gt, a_gt = print_formatted_qa_pairs(questions, answers)
        print('prediction:')
        q_pre, a_pre = print_formatted_qa_pairs(q_output_id_vals, a_output_id_vals)
        qsim, asim = batch_WUPS(q_gt, q_pre, a_gt, a_pre)
        q_bleu, a_bleu = batch_BLEU(q_pre, a_pre, image_ids)
        print("\t\tquestion batch bleu4={:f}, answer batch bleu1={:f}".format(
            q_bleu[-1], a_bleu[0]))
        print('question WUPS@0 and @0.9: ', qsim)
        print('answer WUPS@0 and @0.9: ', asim)
        print('epoch %d, time: %.2f' % (epoch, time.time()-t))

        if epoch % Constant.TEST_EVERY == 0:
            # qa_generator.module.train(False)
            qa_generator.train(False)
            eval_ans = test(epoch, qa_generator, test_data, templates)

        if eval_ans > best_eval:
            torch.save(qa_generator.state_dict(), Constant.MODEL_PATH)
            best_eval = eval_ans


# Test function
def test(epoch, model, test_data, templates, use_gt=False):

    # model
    qa_generator = model

    q_wups = [[], []]
    a_wups = [[], []]
    q_bleu_mean = [[], [], [], []]
    a_bleu_mean = [[], [], [], []]

    for i, inputs in enumerate(test_data):

        num_loops = len(test_data)
        question_templates, answer_templates = templates
        template_indices = random.sample(range(len(question_templates)), Constant.NUM_TEMPS)
        question_templates = [question_templates[idx] for idx in template_indices]
        answer_templates = [answer_templates[idx] for idx in template_indices]
        questions, answers, es_pairs, image_ids = inputs

        results = qa_generator(question_templates,
                               answer_templates,
                               inputs,
                               is_training=False,
                               use_gt=use_gt)

        loss, loss_q, loss_a, loss_t, output_h, output_logits, sampled_qs, \
        a_output_h, a_output_logits, sampled_as, acc = results

        if i in [0, num_loops//3, 2*num_loops//3, num_loops-1]:
            for k in range(len(sampled_qs)):
                if use_gt:
                    q_output = torch.LongTensor(questions)
                else:
                    q_output = sampled_qs[k].data.cpu()
                a_output = sampled_as[k].data.cpu()

                q_pre, a_pre = print_formatted_qa_pairs(q_output, a_output, is_print=False)
                q_gt, a_gt = print_formatted_qa_pairs(questions, answers, is_print=False)
                q_bleu, a_bleu = batch_BLEU(q_pre, a_pre, image_ids)
                for j in range(4):
                    q_bleu_mean[j].append(q_bleu[j])
                    a_bleu_mean[j].append(a_bleu[j])
                qsim, asim = batch_WUPS(q_gt, q_pre, a_gt, a_pre)
                for j in range(2):
                    q_wups[j].append(qsim[j])
                    a_wups[j].append(asim[j])

    print("#" * 10 + " Test Epoch {} ".format(epoch) + "#" * 10)
    print("#" * 10 + " Entity-Atrribute Pairs " + "#" * 10)
    print_batch_pairs(es_pairs)
    print("#" * 10 + " True QA Pairs " + "#" * 10)
    q_gt, a_gt = print_formatted_qa_pairs(questions, answers)
    print("#" * 10 + " Sampled QA Pairs " + "#" * 10)
    q, a = print_formatted_qa_pairs(sampled_qs[0].data.cpu(), sampled_as[0].data.cpu())
    q_bleu, a_bleu = batch_BLEU(q, a, image_ids)
    for j in range(4):
        q_bleu_mean[j].append(q_bleu[j])
        print("Mean of question BLEU", j+1, ": ", np.mean(np.array(q_bleu_mean[j])))
        a_bleu_mean[j].append(a_bleu[j])
        print("Mean of answer BLEU", j+1, ": ", np.mean(np.array(a_bleu_mean[j])))

    qsim, asim = batch_WUPS(q_gt, q, a_gt, a)
    for j in range(2):
        q_wups[j].append(qsim[j])
        a_wups[j].append(asim[j])

    print("Question WUPS@0: ", np.mean(np.array(q_wups[0])))
    print("Question WUPS@0.9: ", np.mean(np.array(q_wups[1])))
    print("Answer WUPS@0: ", max(a_wups[0]))        # np.mean(np.array(a_wups[0])))
    eval_ans = np.mean(np.array(a_wups[1]))
    print("Answer WUPS@0.9: ", max(a_wups[1]))      # eval_ans)

    return eval_ans
