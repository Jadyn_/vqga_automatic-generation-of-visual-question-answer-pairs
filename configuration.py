from os import path
import torch


class Constant:
    USE_DEV = True
    PRINT_INTENTION_PROBS = False
    USE_CUDA = True
    # dataset params
    TRAIN_SPLIT = 0.7
    TEST_SPLIT = 0.15
    VAL_SPLIT = 0.15
    RANDOM_SEED = 1111
    MAX_SEQ_LEN = 16
    MAX_REG_NUM = 32
    MAX_ENTITY_NUM = 3
    NUM_INTENTS = 64  # Number of intentions
    DIM_INTENTS = 300  # Dimension of intentions
    ATTN_DIM = 300  # Dimension of embeddings when calculating attributes
    NUM_PAIRS = 112  # Number of entity-attribute pairs
    DIM_EMBED = 300  # Dimension of word embedding
    HIDDEN_DIM = 300  # Dimension of LSTM hidden state
    REGION_FEATURE_EMBED = 1024  # Dimension of a region feature embedding
    # start of sentence, end of sentence, unknown id
    SOS = 0
    EOS = 1
    UNK = 2
    # first real word id in vocabulary
    FIRST_ID = 3
    LSR_rate = 0

    # model params
    LR = 5e-6  # learning rate
    LR_DECAY = 0.95
    NGRAM_ALPHA = 0.75
    BATCH_NORM_DECAY = 0.99
    GRAD_LIMIT = 0.1
    EMBED_GRAD_LIMIT = 0.1
    VALUE_CLIP = 20
    MOMENTUM = 0.9
    MOMENTUM_DECAY = 0.95
    BATCH_SIZE = 8
    NUM_EPOCH = 3
    L2 = 1e-4
    DROPOUT = True
    DP_RATE = 0.1
    ATTN_REG = 0.5 # Attention Regularisation Constant
    BACKUP_PROB = 0.4  # backup probability for ngram count
    NUM_TEMPS = 0
    DIR_PATH = 'model_data/'
    MODEL_PATH = ''
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    TEST_EVERY = 1


class Path:
    use_dev = Constant.USE_DEV

    ROOT = path.dirname(path.abspath(__file__))
    # input files
    IMG_FEATURE_FILE = path.join(ROOT, 'data/img/refined_image_region_feature.json')
    REGION_GRAPHS_FILE = path.join(ROOT, 'data/visual_genome/region_graphs.json')
    QA_FILE = path.join(ROOT, 'data/visual_genome/question_answers.json')
    MAPPING_FILE = path.join(ROOT, 'data/visual_genome/qa_to_region_mapping.json')
    CONCEPTNET_IN = path.join(ROOT, "data/conceptnet/conceptnet-assertions-5.5.5.csv")

    # output files
    TRAIN_OUTPUT = path.join(ROOT, 'data/text_out/preprocess_result_train.json')
    TEST_OUTPUT = path.join(ROOT, 'data/text_out/preprocess_result_test.json')
    VAL_OUTPUT = path.join(ROOT, 'data/text_out/preprocess_result_val.json')

    TRAIN_ID_OUTPUT = path.join(ROOT, 'data/id_out/id_result_train.json')
    TEST_ID_OUTPUT = path.join(ROOT, 'data/id_out/id_result_test.json')
    VAL_ID_OUTPUT = path.join(ROOT, 'data/id_out/id_result_val.json')

    DEV_TRAIN = path.join(ROOT, 'data/dev/dev_train.json')
    DEV_TEST = path.join(ROOT, 'data/dev/dev_test.json')
    DEV_VAL = path.join(ROOT, 'data/dev/dev_val.json')
    DEV_FEATURE = path.join(ROOT, 'data/dev/dev_image_region_feature.json')

    CONCEPTNET_OUT = path.join(ROOT, "data/dict/conceptnet-out.json")

    WORD2ID_FILE = path.join(ROOT, "data/dict/word2id.json")
    ID2WORD_FILE = path.join(ROOT, "data/dict/id2word.json")

    EMBED_IN = path.join(ROOT, 'data/conceptnet/numberbatch-en-17.06.txt.gz')
    EMBED_OUT = path.join(ROOT, 'data/dict/word_embed')
    WORD_EMBED = path.join(ROOT, 'data/dict/word_embed.npy')

    TF_TRAIN = path.join(ROOT, 'data/tfrecords/train')
    TF_TEST = path.join(ROOT, 'data/tfrecords/test')
    TF_VAL = path.join(ROOT, 'data/tfrecords/val')
    TF_DIR = path.join(ROOT, 'data/tfrecords')
    DEV_TF_DIR = path.join(ROOT, 'data/dev')

    IR_WORD2ID = path.join(ROOT, "data/ir_out/ir_word2id.json")
    IR_ID2WORD = path.join(ROOT, "data/ir_out/ir_id2word.json")
    M4MAT = path.join(ROOT, "data/ir_out/mat_0_9181.dat")
    LOOKUP_TABLE = path.join(ROOT, "data/ir_out/lookup_table.json")
    CLUSTER_PATH = path.join(ROOT, "data/embd_retrieval/cluster/ir_k50_context_hierarchy")

    UNIGRAM_COUNT = path.join(ROOT, "data/dict/unigram_count.dat")
    BIGRAM_COUNT = path.join(ROOT, "data/dict/bigram_count.dat")

    @staticmethod
    def train_file():
        if Path.use_dev:
            return Path.DEV_TRAIN
        else:
            return Path.TRAIN_ID_OUTPUT

    @staticmethod
    def test_file():
        if Path.use_dev:
            return Path.DEV_TEST
        else:
            return Path.TEST_ID_OUTPUT

    @staticmethod
    def val_file():
        if Path.use_dev:
            return Path.DEV_VAL
        else:
            return Path.VAL_ID_OUTPUT

    @staticmethod
    def feature_file():
        if Path.use_dev:
            return Path.DEV_FEATURE
        else:
            return Path.IMG_FEATURE_FILE

    @staticmethod
    def tf_dir():
        if Path.use_dev:
            return Path.DEV_TF_DIR
        else:
            return Path.TF_DIR