import time
from configuration import Constant
from Attention import TopDownAttention, QuestionAttention
from QuestionDecoder import QuestionDecoder
from AnswerDecoder import AnswerDecoder
from JointRepresentation import h_att
import torch
import torch.nn as nn
from torch.autograd import Variable
from utils import temps_label
import numpy as np

class QA_Generator(nn.Module):

    def __init__(self, dictionary, num_temps):
        super(QA_Generator, self).__init__()
        self.dictionary = dictionary
        self.embed_size = Constant.DIM_EMBED
        self.wv = torch.nn.Embedding(self.dictionary.ntoken, self.embed_size, padding_idx=Constant.EOS)
        self.vocab_size = len(self.dictionary)

        self.QA = QuestionAttention(Constant.DIM_EMBED,
                                    Constant.HIDDEN_DIM,
                                    1,
                                    Constant.NUM_PAIRS,
                                    4*Constant.DIM_EMBED,
                                    num_temps,
                                    self.vocab_size).to(Constant.DEVICE)

        self.QD = QuestionDecoder(Constant.DIM_EMBED,
                                  Constant.HIDDEN_DIM,
                                  Constant.MAX_SEQ_LEN,
                                  self.vocab_size,
                                  self.wv,
                                  self.dictionary).to(Constant.DEVICE)

        self.AD = AnswerDecoder(Constant.DIM_EMBED,
                                Constant.HIDDEN_DIM,
                                Constant.MAX_SEQ_LEN,
                                self.vocab_size,
                                self.wv,
                                self.dictionary).to(Constant.DEVICE)

        self.visual_fc = nn.Linear(4 * Constant.DIM_EMBED, Constant.HIDDEN_DIM)
        self.TopDown = TopDownAttention(2 * Constant.HIDDEN_DIM, Constant.HIDDEN_DIM)
        self.joint_fc = nn.Linear(2 * Constant.HIDDEN_DIM, Constant.HIDDEN_DIM)
        self.loss = nn.BCEWithLogitsLoss(reduction='sum')

    def init_embedding(self, np_file):
        weight_init = torch.from_numpy(np.load(np_file)).to(Constant.DEVICE)
        assert weight_init.shape == (self.vocab_size, self.embed_size)
        self.wv.weight.data = weight_init

    def forward(self, question_templates, answer_templates, batch_inputs, is_training=True, use_gt=False):

        t = time.time()
        #### tensorize
        batch_questions, batch_answers, batch_pairs, img_ids = batch_inputs
        batch_size = batch_questions.shape[0]
        question_templates = torch.LongTensor(question_templates).to(Constant.DEVICE)
        answer_templates = torch.LongTensor(answer_templates).to(Constant.DEVICE)

        pair_embedding = self.wv(batch_pairs.view(-1, Constant.NUM_PAIRS*4)).view(-1, Constant.NUM_PAIRS, 4*Constant.HIDDEN_DIM)
        template_emb_q = self.wv(question_templates)  # num_temps * seq_len * emb
        template_emb_a = self.wv(answer_templates)  # num_temps * seq_len * emb

        # print('tensorize time: ', time.time() - t)
        t = time.time()

        templates_score, h_temp, at_emb = self.QA(template_emb_q,
                                                  template_emb_a,
                                                  pair_embedding)  # batch, num_temps

        # print('question context attention time: ', time.time() - t)
        t = time.time()

        if is_training:
            target = torch.from_numpy(temps_label(question_templates, img_ids, batch_size)).float().to(Constant.DEVICE)
            loss_t = self.loss(templates_score, target)
        else:
            loss_t = 0

        # print('cal question label loss time: ', time.time() - t)

        # scores_np = templates_score.data.cpu().numpy()
        # output = torch.from_numpy(np.where(scores_np > 0.5, 1, 0)).to(Constant.DEVICE)
        # acc = (target.eq(output.float())).sum() / (Constant.BATCH_SIZE*Constant.NUM_TEMPS*1e-2)
        acc = 0

        if is_training:
            k = 1
        else:
            k = 1
        _, top_k = torch.topk(templates_score, k)
        batch_templates_q = question_templates[top_k]  # batch * seq_len
        batch_templates_a = answer_templates[top_k]  # batch * seq_len
        top_emb_q = h_temp.squeeze()[top_k]
        top_emb_a = at_emb.squeeze()[top_k]
        sampled_q, sampled_a = [], []

        for sentence in range(k):

            topk_emb_q = top_emb_q[:, sentence, :]
            topk_emb_a = top_emb_a[:, sentence, :]
            topk_batch_q_templates = batch_templates_q[:, sentence, :]
            topk_batch_a_templates = batch_templates_a[:, sentence, :]

            # attend all img features by q_temp
            # q_attn = topk_emb_q.unsqueeze(1).repeat(1, Constant.NUM_PAIRS, 1)  # BATCH, pairs, hid
            # v_attn = self.visual_fc(pair_embedding)
            # hq_0 = self.TopDown(q_attn, v_attn)

            t = time.time()

            hq_0 = self.joint_fc(h_att(pair_embedding, topk_emb_q))

            # print('es, q incorporation time: ', time.time() - t)
            t = time.time()

            #### Get Question Decoder output

            output_h, output_logits, output_ids, loss_q, h = self.QD(hq_0,
                                                                     batch_pairs,
                                                                     pair_embedding.view(-1, 4 * Constant.NUM_PAIRS, Constant.HIDDEN_DIM),
                                                                     topk_emb_q,
                                                                     q_temp=topk_batch_q_templates,
                                                                     q_ids=batch_questions,
                                                                     is_training=is_training,
                                                                     use_gt=use_gt)
            sampled_q.append(output_ids)

            # print('question decode time: ', time.time() - t)

            #### Get Answer Decoder output

            q_emb = output_h
            assert q_emb.size()[1:] == (Constant.MAX_SEQ_LEN, Constant.HIDDEN_DIM)

            # q_attn = q_emb.unsqueeze(1).repeat(1, Constant.NUM_PAIRS, 1)  # BATCH, pairs, hid
            # ha_0 = self.TopDown(q_attn, v_attn)

            t = time.time()

            ha_0 = self.joint_fc(h_att(pair_embedding, q_emb))

            a_output_h, a_output_logits, a_output_ids, loss_a = self.AD(ha_0,
                                                                        batch_pairs,
                                                                        pair_embedding.view(-1, 4 * Constant.NUM_PAIRS, Constant.HIDDEN_DIM),
                                                                        topk_emb_a,
                                                                        a_ids=batch_answers,
                                                                        a_temp=topk_batch_a_templates,
                                                                        is_training=is_training)
            sampled_a.append(a_output_ids)

            # print('answer attn + decode time: ', time.time() - t)

            loss = loss_q + loss_a + loss_t
            if is_training:
                assert loss_t.requires_grad
                assert loss.requires_grad

        return loss, loss_q, loss_a, loss_t, output_h, output_logits, sampled_q, a_output_h, a_output_logits, sampled_a, acc
