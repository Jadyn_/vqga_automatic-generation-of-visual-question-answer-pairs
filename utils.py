
from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction, corpus_bleu
from nltk.util import ngrams
from collections import Counter
from scipy.spatial.distance import cdist as dist
from VGdata import Dictionary
import nltk
# nltk.download('wordnet')
from configuration import Constant
from WUPS import cal_WUPS
import numpy as np
import json
import time
from scipy.spatial.distance import hamming


d = Dictionary.load_from_file('data/VG/VG_dictionary.pkl')
dataset_path = 'data/VG/question_answers.json'
all_data = json.load(open(dataset_path))[:250]


def padList(lst, seq_length, padding):
    if len(lst) < seq_length:
        return lst + [padding]*(seq_length - len(lst))
    return lst[:seq_length]

def list2string(lst):
    return ' '.join([d.idx2word[int(c)] for c in lst])

def string2list(sent):
    return [d.word2idx[w] for w in sent]

def print_batch_pairs(batch_pairs):
    def not_unk_pair(ep):
        UNK = Constant.UNK
        return not np.array_equal(ep, np.array([UNK,UNK,UNK,UNK]))
    ep_pairs = [[[[d.idx2word[int(ep[i])] for i in range(4)] for ep in filter(not_unk_pair, batch)]] for batch in batch_pairs]
    print('\n'.join([str(i)+': ' + str(ep_pairs[i]) for i in range(len(ep_pairs))]))

def get_templates(temp_addr='templates_bigram.json'):
    q_templates, a_templates = [], []
    seq_len = Constant.MAX_SEQ_LEN
    EOS = Constant.EOS
    with open(temp_addr, 'r') as f:
        templates = json.load(f)
    for t in templates:
        q_templates.append(padList(t['questiontoks'], seq_len, EOS))
        a_templates.append(padList(t['answertoks'], seq_len, EOS))
    return q_templates, a_templates


def remove_padding(string):
    lst = string.split()
    for i in range(len(lst)):
        if lst[i] == 'EOS':
            return ' '.join(lst[:i])
    return string

def print_formatted_qa_pairs(batch_q, batch_a, is_print=True):

    def format_strings(s1, s2, index, max_len):
        return str(index)+": "+s1 + " "*(max_len -len(s1)) +  " \t "+str(index)+": " + s2
    q_strings = [remove_padding(list2string(batch_q[i]))+ ' ?' for i in range(len(batch_q))]
    a_strings = [remove_padding(list2string(batch_a[i])) for i in range(len(batch_a))]
    max_q_len = max([len(s) for s in q_strings]) + 10
    if is_print:
        print("\n".join([format_strings(q_strings[i], a_strings[i], i, max_q_len) for i in range(len(q_strings))]))
    return q_strings, a_strings


def remove_spcital_id(inp):
    '''
    :param inp: a list of ids representing a sentence
    :return: the list with all ids that represent start or end of sentence tags removed
    '''
    return list([i for i in inp if i not in {Constant.EOS, Constant.SOS}])


def calc_BLEU(hyp_lst, ref_lst, weights=(0.25, 0.25, 0.25, 0.25), smoothing_function=SmoothingFunction().method1):

    # references = [split_sent(ref) for ref in ref_lst]
    hypotheses = [remove_padding(hyp).split() for hyp in hyp_lst]


    return [corpus_bleu(list_of_references=ref_lst, hypotheses=hypotheses, weights=(1, 0, 0, 0),
                smoothing_function=smoothing_function), \
           corpus_bleu(list_of_references=ref_lst, hypotheses=hypotheses, weights=(0.5, 0.5, 0, 0),
                smoothing_function=smoothing_function), \
           corpus_bleu(list_of_references=ref_lst, hypotheses=hypotheses, weights=(0.33, 0.33, 0.33, 0),
                smoothing_function=smoothing_function), \
           corpus_bleu(list_of_references=ref_lst, hypotheses=hypotheses, weights=weights,
                smoothing_function=smoothing_function)]
#
# def strickBleu(batch_ids, q_output_ids, a_output_ids, q_batch_ids2ref_lst, a_batch_ids2ref_lst):
#     """
#     Equation 18 and 19.
#     :param batch_ids: id list for the questions/answers
#     :param q_output_ids: the hypothesis question in id form
#     :param a_output_ids: the hypothesis answer in id form
#     :param q_batch_ids2ref_lst: a dict mapping batch_ids to the reference questions
#     :param a_batch_ids2ref_lst: a dict mapping batch_ids to the reference answers
#     :param weights: how much weight to give each ngram
#     :param smoothing_function: how to smooth the average for if there are no ngrams of a particular n
#     :return:
#     """
#
#
#     p_numerators = 0  # Key = ngram order, and value = no. of ngram matches.
#     p_denominators = 0  # Key = ngram order, and value = no. of ngram in ref.
#     list_of_references_questions = q_batch_ids2ref_lst(batch_ids)
#     list_of_references_answers = a_batch_ids2ref_lst(batch_ids)
#     hypotheses_question = [remove_spcital_id(i) for i in q_output_ids]
#     hypotheses_answers = [remove_spcital_id(i) for i in a_output_ids]
#     best =0
#     result =[]
#     for references_qs,references_as, hypothesis_q,hypotheses_a in zip(list_of_references_questions,list_of_references_answers,hypotheses_question,hypotheses_answers):
#         for reference_q,reference_a in zip(references_qs,references_as):
#             for i in range(1,5): # 1 to 4 ngrame
#                 q_numerator,q_denominator = m_precision([reference_q], hypothesis_q, i)
#                 a_numerator,a_denominator = m_precision([reference_a],hypotheses_a,i)
#                 p_numerators += q_numerator+a_numerator
#                 p_denominators += q_denominator+a_denominator
#             score = p_numerators/p_denominators
#             if best <= score:
#                 best = score
#             p_denominators,p_numerators = 0,0
#         result.append(best)
#         best = 0
#     return sum(result)/len(result)
#
#
#
# def m_precision(references, hypothesis, n):
#     """
#     :param references: A list of reference translations.
#     :type references: list(list(str))
#     :param hypothesis: A hypothesis translation.
#     :type hypothesis: list(str)
#     :param n: The ngram order.
#     :type n: int
#     """
#     counts = Counter(ngrams(hypothesis, n)) if len(hypothesis) >= n else Counter()
#     # get a union of references' counts.
#     max_counts = {}
#     for reference in references:
#         reference_counts = Counter(ngrams(reference, n)) if len(reference) >= n else Counter()
#         for ngram in counts:
#             max_counts[ngram] = max(max_counts.get(ngram, 0),
#                                     reference_counts[ngram])
#     # Assigns the intersection between hypothesis and references' counts.
#     clipped_counts = {ngram: min(count, max_counts[ngram])
#                       for ngram, count in counts.items()}
#
#     numerator = sum(clipped_counts.values())
#     # Ensures that denominator is minimum 1 to avoid ZeroDivisionError.
#     denominator = max(1, sum(counts.values()))
#     return numerator,denominator

def clip_grad_value_(parameters, clip_value):
    r"""Clips gradient of an iterable of parameters at specified value.

    Gradients are modified in-place.

    Arguments:
        parameters (Iterable[Tensor] or Tensor): an iterable of Tensors or a
            single Tensor that will have gradients normalized
        clip_value (float or int): maximum allowed value of the gradients
            The gradients are clipped in the range [-clip_value, clip_value]
    """
    clip_value = float(clip_value)
    for p in filter(lambda p: p.grad is not None, parameters):
        p.grad.data.clamp_(min=-clip_value, max=clip_value)


def acc_eval(output_id, label):
    def remove_padding(sent):
        for i in range(len(sent)):
            if sent[i] == 1:
                return sent[:i]
        return sent

    single_acc = []
    for k in range(len(label)):
        ref = np.asarray(remove_padding(label[k])).reshape((1, -1))
        hyp = np.asarray(output_id[k][:len(ref[0])]).reshape((1, -1))
        d = dist(hyp, ref, 'hamming')
        single_acc.append(d)
    return 1.0 - np.mean(single_acc)

def batch_BLEU(q, a, img_ids):
    q_refs = get_all_refs(img_ids, 'question')
    q_bleu = calc_BLEU(q, q_refs)
    a_refs = get_all_refs(img_ids, 'answer')
    a_bleu = calc_BLEU(a, a_refs)
    # strict_bleu = strickBleu(full_ids, full_q_output_ids, full_a_output_ids, test_ref_q_fn, test_ref_a_fn)
    # print("#" * 10 + " question corpus bleu4={:f}, answer corpus bleu1={:f}".format(
    #     q_bleu[-1], a_bleu[0]) + "#" * 10)
    return q_bleu, a_bleu

def batch_WUPS(q_gt, q_pre, a_gt, a_pre):
    q_WUPS = [cal_WUPS(q_gt, q_pre), cal_WUPS(q_gt, q_pre, thresh=0.9)]
    a_WUPS = [cal_WUPS(a_gt, a_pre), cal_WUPS(a_gt, a_pre, thresh=0.9)]
    return q_WUPS, a_WUPS

def split_sent(sentence):
    sentence = sentence.lower()
    sentence = sentence.replace(',', '').replace('?', '').replace('\'s', ' \'s')
    return sentence.split()

def get_all_refs(img_ids, name):
    assert name in ['question', 'answer']
    batch_refs = []
    for img_id in img_ids:
        batch_refs.append([split_sent(qa[name]) for qa in all_data[img_id - 1]['qas']])
    return batch_refs

def temps_label(temps, img_ids, batch_size, thres=0.5):
    """

    :param temps: all templates: num_temps * seq_len
    :param sents: batch questions: batch_size * num_questions (various for diff img) * seq_len
    :param img_ids: batch ids
    :param thres:
    :return: true classification labels, batch_size * num_temps
    """

    labels = np.zeros((batch_size, Constant.NUM_TEMPS))
    # weights = (0.33, 0.33, 0.33, 0.)
    q_lists = get_all_refs(img_ids, 'question')

    # t = time.time()
    # for i, img_id in enumerate(img_ids):  # for each image in batch
    #     refs = q_lists[i]
    #     for j, temp in enumerate(temps):
    #         hyp = [d.idx2word[int(id)] for id in temp if id != 1]
    #         score = corpus_bleu(list_of_references=[refs],
    #                             hypotheses=[hyp],
    #                             weights=weights,
    #                             smoothing_function=SmoothingFunction().method1)
    #         if score > thres:
    #             labels[i, j] = 1
    # print('bleu: ', time.time() - t)

    # t = time.time()
    for i, img_id in enumerate(img_ids):  # for each image in batch
        refs = [padList(string2list(q), 10, 1) for q in q_lists[i]]
        for j, temp in enumerate(temps):
            hyp = [padList(temp, 10, 1).tolist()] * len(refs)
            score = 1 - np.sum(np.equal(refs, hyp), axis=1) / 10
            if min(score) < 0.4:
                labels[i, j] = 1
    # print('manual: ', time.time() - t)
    #
    # labels = np.zeros((Constant.BATCH_SIZE, Constant.NUM_TEMPS))
    #
    # t = time.time()
    # for i, img_id in enumerate(img_ids):  # for each image in batch
    #     refs = q_lists[i]
    #     for j, temp in enumerate(temps):
    #         hyp = [d.idx2word[int(id)] for id in temp]
    #         hyp = padList(hyp, 10, 'EOS')
    #         for ref in refs:
    #             score = hamming(padList(ref, 10, 'EOS'), hyp)
    #             if score < 0.4:
    #                 labels[i, j] = 1
    #                 break
    # print('hamming: ', time.time() - t)


    return labels
