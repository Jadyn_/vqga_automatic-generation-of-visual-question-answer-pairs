
import os
from configuration import Constant
from utils import *
from Model import *
from VGdata import Dictionary, VGDataset
from torch.utils.data import DataLoader, RandomSampler
import argSet
import Train


def load_args():

    args = argSet.parse_args()
    Constant.DP_RATE = args.dropout
    Constant.NUM_EPOCH = args.epochs
    Constant.BATCH_SIZE = args.batch_size
    Constant.TEST_EVERY = args.test_every
    Constant.LR = args.lr
    Constant.HIDDEN_DIM = args.num_hid
    Constant.DIM_EMBED = Constant.HIDDEN_DIM
    dataset = args.dataset
    pretrain = args.pretrain
    Constant.MAX_SEQ_LEN = args.seq_len
    output_dir = 'saved_model/'
    model_path = os.path.join(output_dir, 'model.pth')
    Constant.MODEL_PATH = model_path
    Constant.NUM_TEMPS = args.num_tpl
    if dataset == 'full':
        Constant.USE_DEV = False
        Constant.NUM_PAIRS = 50
    elif dataset == 'dev':
        Constant.USE_DEV = True
        Constant.NUM_PAIRS = 30
    else:
        print('Invalid Dataset')
        exit()

    summary_str = "Dropout Rate={}, Num Epochs={}, " \
                  "Batch_Size={}, Test Every={}, Learning Rate={}, Dataset={}, Device={}, Pretrain={}, Use {} templates".format(
        Constant.DP_RATE, Constant.NUM_EPOCH, Constant.BATCH_SIZE, Constant.TEST_EVERY, Constant.LR, dataset, Constant.DEVICE, pretrain, Constant.NUM_TEMPS)
    print(summary_str)


if __name__ == "__main__":

    torch.multiprocessing.set_start_method('spawn', force="True")
    print('initializing...')
    load_args()

    dictionary = Dictionary.load_from_file('data/VG/VG_dictionary.pkl')
    train_dset = VGDataset('train', dictionary, use_dev=Constant.USE_DEV)
    test_dset = VGDataset('test', dictionary, use_dev=Constant.USE_DEV)
    train_loader = DataLoader(train_dset, Constant.BATCH_SIZE, shuffle=True, num_workers=1, drop_last=True)
    test_loader = DataLoader(test_dset, Constant.BATCH_SIZE, shuffle=True, num_workers=1, drop_last=True)
    templates = get_templates('data/VG/temp_VG.json')
    print('finish loading data.')

    # initialise Model
    qa_generator = QA_Generator(dictionary, Constant.NUM_TEMPS).to(Constant.DEVICE)
    qa_generator.init_embedding('data/glove42b_weights_300d.npy')
    model = nn.DataParallel(qa_generator, dim=0).to(Constant.DEVICE)
    # model = qa_generator

    print('model initialized!')

    Train.train(model, train_loader, test_loader, templates)
