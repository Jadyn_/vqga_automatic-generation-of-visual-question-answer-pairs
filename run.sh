#!/bin/bash
#SBATCH --account=da33
#SBATCH --get-user-env
#SBATCH --job-name="VGQA"
#SBATCH --time=96:00:00
#SBATCH --ntasks=16
#SBATCH --mem=64GB
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:V100:2
#SBATCH --partition=m3g

#module load tensorboardx/1.2.0-py36-cuda90
#module load pytorch/0.4.1-py36-cuda90
#module load pytorch/1.0-cuda10

export PROJECT=da33
export CONDA_ENVS=/projects/$PROJECT/$USER/conda_envs

#mkdir -p $CONDA_ENVS

module load anaconda

#conda create --yes -p $CONDA_ENVS/VGQA_env

# To use the environment
source activate $CONDA_ENVS/VGQA_env

#conda install nltk
#conda install pytorch torchvision

echo "------git branch------"
git branch | cat
echo "------last commit------"
git log -n 1 | cat
echo "------output------"

#python -m torch.utils.bottleneck Main.py 
python -u Main.py --epochs=50 --dataset=dev --batch_size=128 --lr=5e-6 --dropout=0.1
